# coding: utf-8

from node import Node
from chained_list import ChainedList

def test_print_node():
    """ Create a list of number. """
    n_1 = Node(2)
    n_2 = Node(5)
    n_3 = Node(10)
    n_4 = Node(1)
    n_1.link = n_2
    n_2.link = n_3
    n_3.link = n_4
    return n_1.__str__()

if __name__ == "__main__":
    test_print_node()
    LIST = ChainedList(test_print_node())
    LIST.insert_node_after(2, 8)
    LIST.list_order()
    LIST.delete_node(5)
    LIST.delete_node(5)
