# coding: utf-8
class ChainedList:
    """
    Chained list Object
    Parameters
    ----------
    nodes : list
    list that we want to transfert in a chained list of Node object """
    def __init__(self, nodes):
        self.nodes = nodes
        print("The list is ", self.nodes)

    def insert_node_after(self, data, new_node):
        """ Insert a new node after the node with the value == data
            Parameters
            ----------
            data : searched data
            new_node : node to insert """

        if data not in self.nodes:
            print(data, " isn't in list. ")
        if data in self.nodes:
            position = self.nodes.index(data)
            self.nodes.insert(position+1, new_node)  # insert after the node
            print("The list after insert number", new_node, " after number ",
                  data, " is ", self.nodes)

    def delete_node(self, data):
        """ Delete all node(s) value == data
            Parameters
            ----------
            data : searched data to delete """
        if data not in self.nodes:
            print(data, " isn't in list.")
        elif data in self.nodes:
            while data in self.nodes:
                self.nodes.remove(data)
            print("The list after delete number", data, " is ", self.nodes)

    def list_order(self):
        """ Arrange the order of list elements """
        self.nodes.sort()
        print("The list after arrange ordre is ", self.nodes)
