# INLO TP3 P.O.O.

An exercise of Object-Oriented Programming in python3.

The objective is to write a programme to operate a number list by add or delete the element in it.

# Description

The node.py is used to create a list of number by add element individually.

The chained_list.py is used to insert or delete a number and arrange number order in the list.

# How to use

Download node.py, chained_list.py and start_node.py.

Run start_node.py to test function.