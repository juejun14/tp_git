# coding: utf-8
class Node:
    """ Create a list of number by add element individually. """
    def __init__(self, param_data):
        self.data = param_data
        self.link = None

    def __str__(self):
        liste = []
        node = self
        while node.link is not None:
            liste.append(node.data)
            node = node.link
        liste.append(node.data)
        liste.sort()
        return liste
