# -*- coding: utf-8 -*-
"""
Created on Wed Feb  9 11:27:28 2022

@author: Juejun
"""

import unittest
from chained_list import *

class TestLinkList(unittest.TestCase):
    """ Test functions of ChainedList class """
    def setUp(self):
        """ initialization of empty linked lists """
        self.test_list = ChainedList()
        self.copy = self.test_list

    def test_equals(self):
        """test if two empty list are equals."""
        self.assertEqual(self.copy, self.test_list)

    def test_isempty(self):
        """test if list is empty."""
        self.assertIsNone(self.test_list.first_node)

    def test_add_node(self):
        """test if list is not empty after add a node. """
        self.copy.add_node(7)
        self.assertIsNotNone(self.copy.first_node)

    def test_add_delete_node_are_equals(self):
        """test if two list equal after delete a node. """
        self.copy.delete_node(7)
        self.assertEqual(self.test_list, self.copy)

    def test_add_is_top(self):
        """test if add element 'e' is the top of list."""
        self.copy.add_node('e')
        self.copy.insert_node_after('e', 8)
        self.assertEqual(self.copy.first_node, 'e')

if __name__ == "__main__":
	unittest.main()
