from view_obj import View
from model import Ensemble
from model import Person

class Controller():
    """main file to generate model file and view file."""
    def __init__(self):
        """initialization"""
        self.view = View(self)
        self.model = Ensemble()

    def start_view(self):
        """create and show the window."""
        self.view.create_fields()
        self.view.main()

    def insert(self):
        """ insert data """
        check_result = self.view.check_entry()

        person = Person(self.view.get_value("Nom"),
                        self.view.get_value("Prénom"),
                        self.view.get_value("Téléphone"),
                        self.view.get_value("Adresse"),
                        self.view.get_value("Ville"))
        if check_result is not False:
            result = self.model.insert_person(person)
            self.view.result[1] = str(result)
            self.view.result_window()

    def search(self):
        """ search if data exists """
        check_result = self.view.check_entry()
        if check_result is not False:
            name = self.view.get_value("Nom")
            result = self.model.search_person(name)
            print(result)
            if result:
                self.view.result[1] = str(result)
                self.view.result_window()

    def delete(self):
        """ deleted data """
        self.view.check_entry()

        nom = self.view.get_value("Nom")
        prenom = self.view.get_value("Prénom")
        if self.view.get_value("Nom") != '':
            answer = self.view.check_message()
            if answer:
                result = self.model.delete_person(nom, prenom)
                self.view.result[1] = str(result)
                self.view.result_window()
            else:
                pass

    def closewindow(self):
        """ stop program."""
        answer = self.view.check_message()
        if answer:
            self.view.destroy()

    def button_press_handle(self, button_id):
        """Manage click events """
        print("[Controller][button_press_handle] "+ button_id)
        if button_id == "Inserer":
            self.insert()
        elif button_id == "Chercher":
            self.search()
        elif button_id == "Supprimer":
            self.delete()
        elif button_id == "Quitter":
            self.model.copy_data()
            self.closewindow()
        else:
            pass

if __name__ == "__main__":
    CONTROLLER = Controller()
    CONTROLLER.start_view()
