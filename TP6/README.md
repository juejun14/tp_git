# INLO TP6 MVC


# Description

This document contains 3 python files: model.py, view_obj.py and controller.py.

model.py contains two class, a class is to create a list of person, another is to create a dictionary by the lastname of person based on  list. This second class is also used to manipulate the dictionary.

view_obj.py is used to create the window of application.

controller.py connects the model and the view. All the functions will be called by controller.

# How to use

## Create a new repository

git clone git@gitlab.com:juejun14/tp6_mvc.git

cd tp6_mvc

Run the controller.py file.

The txt file is included to test basic functionality.

You can search, type, or delete its contents.

When you exit the program, a txt file is automatically generated to store the contents of the operation just now. 
