# coding: utf-8

from tkinter import Button
from tkinter import Entry
from tkinter import Label
from tkinter import StringVar
from tkinter import Tk
from tkinter import messagebox
from tkinter import Message
from tkinter import Frame
"""
Class View of the project

@author : CHEN Juejun
"""
class View(Tk):
    """create a window"""
    def __init__(self, controller):
        """initialization"""
        super().__init__()
        self.controller = controller
        self.widgets_labs = {}
        self.widgets_entry = {}
        self.widgets_button = {}
        self.entries = ["Nom", "Prénom", "Téléphone", "Adresse", "Ville"]
        self.buttons = ["Chercher", "Inserer", "Supprimer", "Quitter"]
        self.result = ["Résultat", ""]
        self.result_frame = Frame(self)

    def get_value(self, key):
        """return key value"""
        return self.widgets_entry[key].get()

    def create_fields(self):
        """create labels and buttons of interface."""
        global i
        i, j = 0, 0

        for idi in self.entries: #create labals
            lab = Label(self, text=idi.capitalize())
            self.widgets_labs[idi] = lab
            lab.grid(row=i, column=0, sticky='w')

            var = StringVar() #create entry field for each label
            entry = Entry(self, text=var)
            self.widgets_entry[idi] = entry
            entry.grid(row=i, column=1, columnspan=len(self.buttons)-1)

            i += 1

        for idi in self.buttons:
            button_w = Button(self, text=idi,
                              command=(lambda button=idi: self.controller.button_press_handle(button)))
            self.widgets_button[idi] = button_w
            button_w.grid(row=i, column=j)

            j += 1

    def check_message(self):
        """ask for confirmation."""
        answer = messagebox.askyesno('Attention', 'Vous êtes sur(e)?')
        return answer

    def check_entry(self):
        """check if the entry is not correct."""
        if self.get_value("Nom") == '':
            messagebox.showerror('Error', 'Le Nom est nécéssaire!')
            return False
        if self.get_value("Nom"):
            name = self.get_value("Nom")
            if name.isalpha() is False:
                messagebox.showerror('Error', 'Veuillez remplir des lettres.')
                return False
        if self.get_value("Prénom"):
            name = self.get_value("Prénom")
            if name.isalpha() is False:
                messagebox.showerror('Error', 'Veuillez remplir des lettres.')
                return False
        if self.get_value("Téléphone"):
            number = self.get_value("Téléphone")
            if number.isdigit() is False:
                messagebox.showerror('Error', 'Veuillez remplir des chiffres.')
                return False
        else:
            return True

    def result_window(self):
        """show result if click event"""
        k = i
        self.result_frame.destroy()
        self.result_frame = Frame(self)
        self.result_frame.grid(row=k+1, columnspan=len(self.buttons))

        lab = Label(self.result_frame, text=self.result[0])
        self.widgets_labs[self.result[0]] = lab
        lab.pack()

        msg = Message(self.result_frame, text=self.result[1])
        self.widgets_labs[self.result[1]] = lab
        msg.pack()

    def main(self):
        """execute the code"""
        print("[View] main")
        self.title("Annuaire")
        self.mainloop()
