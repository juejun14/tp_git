#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pickle

class Person:
    """create a list for each person."""
    def __init__(self, nom, prenom, telephone='', adresse='', ville=''):
        """Initialization of person"""
        self.nom = nom.lower()
        self.prenom = prenom.lower()
        self.telephone = telephone
        self.adresse = adresse
        self.ville = ville

    def get_nom(self):
        """return lastname"""
        return self.nom

    def get_prenom(self):
        """return firstname"""
        return self.prenom

    def get_telephone(self):
        """return telephone number"""
        return self.telephone

    def get_adresse(self):
        """return adresse"""
        return self.adresse

    def get_ville(self):
        """return location"""
        return self.ville

    def __str__(self):
        """set string return"""
        return ("Nom: "+ self.get_nom() + " Prénom: " + self.get_prenom()
                + " Téléphone: "+ self.get_telephone()
                + " Adresse: " + self.get_adresse()
                + " Ville: " + self.get_ville())

class Ensemble:
    """class for manipulating lists """
    def __init__(self):
        """Initialization"""
        self.list_person = {}
        self.file = ''
        try:
            with open('list_to_show.txt', 'rb') as self.file:
                self.list_person = pickle.load(self.file)
            print(self.list_person)
        except:
            print("Pas de liste enregistrée")

    def insert_person(self, person):
        """insert data to dictionary"""

        prenom = person.get_prenom()
        nom = person.get_nom()
        show_info = ''
        if nom not in self.list_person.keys():
            self.list_person.setdefault(f"{nom}", []).append(f"{person}")
            show_info = self.list_person[nom]
        elif nom in self.list_person.keys() and prenom in str(self.list_person[nom]):
            show_info = ("Cette personne existe déjà.")
        elif nom in self.list_person.keys() and prenom not in str(self.list_person[nom]):
            self.list_person[f"{nom}"].append(f"{person}")
            show_info = self.list_person[nom][-1]
        return show_info

    def search_person(self, name):
        """search the person by lastname in dictionary and return the values"""
        name = name.lower()
        names_fetched = "On ne trouve pas cette personne."
        for key, value in self.list_person.items():
            if name in key:
                names_fetched = value
            else:
                pass
        return names_fetched

    def delete_person(self, lastname, firstname=''):
        """search if this person exists and delete it."""
        find_person = self.search_person(lastname)
        print(find_person)
        if isinstance(find_person, str):
            show_info = "Cette personne n'existe pas."
        else:
            key_to_del = []
            values_to_del = []
            show_info = ''
            for key, value in self.list_person.items():
                if lastname in key:
                    key_to_del.append(lastname)
                    values_to_del = value

            if len(values_to_del) == 1:
                for to_del in key_to_del:
                    del self.list_person[to_del]
                    show_info = 'Deleted: ' + str(values_to_del)

            elif len(values_to_del) > 1 and firstname != '':
                value_list = self.list_person[lastname]
                for value in value_list:
                    if firstname in str(value):
                        position = value_list.index(value)
                        values_to_del = value_list[position]
                        value_list.pop(position)
                        self.list_person[lastname] = value_list
                        show_info = 'Deleted: '+ str(values_to_del)
                    else:
                        show_info = "Cette personne n'existe pas."

            elif len(values_to_del) > 1 and firstname == '':
                show_info = "Il existe plusieurs personnes, Veuillez remplir le prénom"

            elif len(values_to_del) == 1:
                for to_del in key_to_del:
                    del self.list_person[to_del]
                    show_info = 'Deleted: ' + str(values_to_del)
        return show_info

    def __str__(self):
        """return content """
        return str(self.list_person.items())

    def copy_data(self):
        """create a pickle file to save content"""
        self.file = self.list_person
        list_to_save = open('list_to_show.txt', 'wb')
        pickle.dump(self.file, list_to_save)
        list_to_save.close()


