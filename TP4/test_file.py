# -*- coding: utf-8 -*-
"""
Created on Mon Jan 31 20:58:00 2022

@author: Juejun
"""
from class_total import *

if __name__ == "__main__":
    test = university('AMU','Sciences')
    test.get_department()  
    
    subject1 = subjects('biologie')
    subject1.get_number_people()   
    
    student2 = person('AAA','Bbbb')
    student2.get_name()    

    teacher1 = teachers('A','B','teacher')
    teacher1.get_name()
    
    student1 = students('CHEN','Juejun','student')
    student1.get_name() 
    student1.choose_subject('biologie')
                            

 