import argparse
import sys
import pandas as pd
from adn import purify_adn, is_valid


def create_parser():
    """ Declares new parser and adds parser arguments """
    program_description = ''' reading fasta file and checking sequence format '''
    parser = argparse.ArgumentParser(add_help=True,
                                     description=program_description)
    parser.add_argument('-i', '--inputfile', default=sys.stdin,
                        help="required input file in fasta format",
                        type=argparse.FileType("r"), required=True)
    return parser

def create_list(fasta):
    """ Change fasta sequence into list """
    for i in range(0, len(fasta)):
        fasta[i] = purify_adn(fasta[i])
        fasta_list = list(filter(None, fasta))
    return fasta_list

def list_to_dict(fasta_list):
    """ 
    Create a dictionary based on fasta sequence list,
    and concatenate multiple values to only one value.
    """
    dict_seq = {}
    for i in range(0, len(fasta_list)):
        if ">" in fasta_list[i]:
            dict_seq.setdefault(fasta_list[i], [])
            for value in fasta_list[i+1:]:
                if ">" not in value:
                    dict_seq[fasta_list[i]].append(value)
                if ">" in value:
                    break
    for key, value in dict_seq.items():
        dict_seq[key] = ''.join(value)
    return dict_seq

def valid_sequence(dict_seq):
    """ Check each sequence and create a valid table and an invalid table. """
    valid_list = []
    seq_length = []
    invalid_list = []
    invalid_charact = []
    invalid_position = []
    for key, value in dict_seq.items():
        if is_valid(value):
            valid_list.append(key)
            seq_length.append(len(value))
        elif not is_valid(value):
            for i in value:
                if i not in ['a', 'c', 't', 'g']:
                    invalid_list.append(key)
                    invalid_charact.append(i)
                    invalid_position.append(value.find(i))
    valid_data = {"Valid sequence": valid_list,
                  "Length": seq_length}
    valid_df = pd.DataFrame(valid_data)
    
    invalid_data = {"Invalid sequence": invalid_list,
                    "Error letter" : invalid_charact,
                    "Position" : invalid_position}
    invalid_df = pd.DataFrame(invalid_data)
    print(valid_df)
    print(invalid_df)

def check_file(fasta_list):
    """ Check whether inputfile is valid. """
    if ">" not in fasta_list[0]:
        print("Please input a fasta file.")
    if ">" in fasta_list[0]:
        dict_seq = list_to_dict(fasta_list)
        valid_sequence(dict_seq)

def main():
    """ Main function for reading fasta file and checking sequence format """
    parser = create_parser()
    args = parser.parse_args()
    args = args.__dict__
    print(args["inputfile"])
    fasta = args["inputfile"].readlines()
    fasta_list = create_list(fasta)
    check_file(fasta_list)

if __name__ == "__main__":
    main()
