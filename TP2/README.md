A parser to check fasta file.

# Description

adn.py to check whether sequence is valid.

# How to use this parser

exemple:

python3 select_fasta_parser.py -i exemple.fasta

It will return the valid sequence list and its length,
and the invalid sequence list.
