#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    A script to check chain of DNA sequence.
"""

__author__ = 'CHEN Juejun'

   
def purify_adn(adn_str):
    try:
        adn_str = adn_str.lower()
        adn_str = adn_str.replace("\n","")  
        adn_str = adn_str.replace(" ","")
    except:
        adn_str = adn_str
    return adn_str

def is_valid(adn_str):
    adn_str = purify_adn(adn_str)
    nucleotide = {'a', 'c', 'g', 't'}
    liste1 = set(list(adn_str))
    if (liste1 == nucleotide) == True:
        return True
    else:
        return False

def get_valid_adn(prompt="Entry your sequence: "):
    adn_str = input(prompt)
    while (is_valid(adn_str)) == False:
        adn_str = input("A invalid sequence, please rewrite your sequence :")
    while (is_valid(adn_str)) == True:
        print("A valid sequence :", purify_adn(adn_str))
        adn_str = input("Entry your sequence :")

#A change was made in doc

#parent of e96c5f4... Call function
